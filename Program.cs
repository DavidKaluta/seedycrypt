﻿/*
CREATED BY DAVID KALUTA
@CDM1337isHOT
Copyright (c) 2018 David Kaluta

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 */
using System;

namespace JustAProgram
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Insert some text: ");
            string s = Console.ReadLine();
            int[] arr;
            string newS;
            var r = new Random();
            Console.Write("Do you want to encrypt or decrypt the text? ");
            string selection=Console.ReadLine();
            selection.ToLower();
            switch(selection){
                case "encrypt":
                    arr=new int[s.Length];
                    for(int i=0;i<arr.Length;i++){
                        if((s[i]>='A' && s[i]<='Z')||(s[i]>='a' && s[i]<='z'))
                            arr[i]=r.Next(0,26);
                        else if(s[i]>='0' && s[i]<='9')
                            arr[i]=r.Next(0,9);
                        else
                            arr[i]=0;
                    }
                    newS=Encrypt(s,arr);
                    Console.Write("The result is {0}.\n",newS);
                    Console.Write("The key is");
                    for(int i=0;i<arr.Length;i++){
                        Console.Write(" {0}",arr[i]);
                    }
                    Console.Write(".\n");
                    break;
                case "decrypt":
                    arr=new int[s.Length];
                    for(int i=0;i<arr.Length;i++){
                        Console.Write("Insert a number: ");
                        arr[i]=int.Parse(Console.ReadLine());
                    }
                    newS=Decrypt(s, arr);
                    Console.Write("The result is {0}.\n",newS);
                    break;
            }
        }
        static string Encrypt(string s, int[] key){
            string newS="";
            char ch;
            int chAsInt;
            for(int i=0;i<s.Length;i++)
            {
                ch=s[i];
                if(ch>='A' && ch<='Z'){
                    ch-='A';
                    chAsInt=(int)ch;
                    chAsInt+=key[i];
                    chAsInt%=26;
                    ch=(char)chAsInt;
                    ch+='A';
                }else if(ch>='a' && ch<='z')
                {
                    ch-='a';
                    chAsInt=(int)ch;
                    chAsInt+=key[i];
                    chAsInt%=26;
                    ch=(char)chAsInt;
                    ch+='a';
                }else if(ch>='0' && ch<='9'){
                    ch-='0';
                    chAsInt=(int)ch;
                    chAsInt+=key[i];
                    chAsInt%=10;
                    ch=(char)chAsInt;
                    ch+='0';
                }
                newS+=ch;
            }
            return newS;
        }
        static string Decrypt(string s, int[] key){
            string newS="";
            char ch;
            int chAsInt;
            for(int i=0;i<s.Length;i++)
            {
                ch=s[i];
                if(ch>='A' && ch<='Z'){
                    ch-='A';
                    chAsInt=(int)ch;
                    chAsInt-=key[i];
                    if(chAsInt<0)
                        chAsInt+=26;
                    ch=(char)chAsInt;
                    ch+='A';
                }else if(ch>='a' && ch<='z')
                {
                    ch-='a';
                    chAsInt=(int)ch;
                    chAsInt-=key[i];
                    if(chAsInt<0)
                        chAsInt+=26;
                    ch=(char)chAsInt;
                    ch+='a';
                }else if(ch>='0' && ch<='9'){
                    ch-='0';
                    chAsInt=(int)ch;
                    chAsInt-=key[i];
                    if(chAsInt<0)
                        chAsInt+=10;
                    ch=(char)chAsInt;
                    ch+='0';
                }
                newS+=ch;
            }
            return newS;
        }
    }
}
